
import socket
import data
#every fanction will use this fanction otherlly so ervery time it will look diffrant(not like this)




def orgenizeData():
    """
    organize the data from the file into a list, and two dictinary
    :return: album_name: a list
            album_songs:a dictinary that looks like "album name":["song1","song2"...]
            songs: a dictinary that looks like "song1":(length,lyrics)
    """
    file_path = "Pink_Floyd_DB.txt"
    album_names = []
    album_songs = {}
    songs = {}
    album_counter = 0
    with open(file_path, "r") as text:
        text = text.read()
    text = text.split("::")
    for index, info in enumerate(text):
        if "#" in info:
            # in the file # separate them or ::
            Falbum = info.split("#")
            album_names.append(Falbum[1])
            album_counter += 1
            album_songs[album_names[album_counter - 1]] = []  # adds 1 more
        elif "*" in info:
            Fsong = info.split("*")
            # name of album:songs
            album_songs[album_names[album_counter - 1]].append(Fsong[1])
            Flyrics = text[index + 3].split("*")
            # name of song       length        lyrics
            songs[Fsong[1]] = (text[index + 2], Flyrics[0])
    return album_names,album_songs,songs


def main():
    SERVER_PORT = 4242
    F_MASSAGE = "welcome to our pink fluid knowledge protocol, we hope you have the worst time using  him:)\nEXIT to leave"
    FWRONG_RESPONSE = "what are you an idiot? just ask HELO!!"
    MENU = "menu:\n1.albumList()-print list of albums\n2.albumSongs(albumName)-print a list of all the album’s songs\n3.songLen(songName)-print the length of the song\n4.songWords(songName)-print all the song’s words\n5.whatAlbum(songName)-print the album of the song\n6.nameSearch(word)-print all the songs that have this word in their name\n7.wordSearch(word)-print all the songs that have this word in their lyrics\n8.mostPopular()\n9.longest()\n10.exit()-to leave:("
    # menu:
    # albumList()-print list of albums
    # albumSongs(albumName)-print a list of all the album’s songs
    # songLen(songName)-print the length of the song
    # songWords(songName)-print all the song’s words
    # whatAlbum(songName)-print the album of the song
    # nameSearch(word)-print all the songs that have this word in their name
    # wordSearch(word)-print all the songs that have this word in their lyrics
    # exit()-leave the program
    EXIT_MSG = "EXIT#thank you and have a wonderful day"
    TYPE_EROR = "ERORR#sorry, coudn't find what you are looking for, check your writing and send it again:)!"
    album_names,album_songs,songs=orgenizeData()
    with socket.socket() as listening_sock:
        server_address = (("", SERVER_PORT))
        listening_sock.bind(server_address)
        cone = True
        while (cone):
            listening_sock.listen(1)
            client_soc, client_addres = listening_sock.accept()
            with client_soc as sock:
                try:
                    sock.sendall(F_MASSAGE.encode())
                    client_ms = client_soc.recv(1024)
                    client_ms = client_ms.decode()
                    while (client_ms != "HELO?"):
                        if (client_ms[:4] == "EXIT"):
                            sock.sendall(EXIT_MSG.encode())
                            return  # so the program stop:)
                        else:
                            sock.sendall(FWRONG_RESPONSE.encode())
                            client_ms = sock.recv(1024)
                            client_ms = client_ms.decode()
                    con = True
                    while (con):
                        #try:
                            sock.sendall(MENU.encode())
                            client_msg = sock.recv(1024)
                            client_msg = client_msg.decode()
                            F_part, S_part = client_msg.split("#")
                            if F_part == "GET" and S_part[-1] == ")":
                                S_part = S_part.split("(")
                                # list out if of range=typeEror
                                S_part[1] = S_part[1][:-1]
                                # coudnt find switch cases:(
                                answer_option={"albumList":data.albumList(album_names),
                                                "albumSongs":data.albumSongs(album_songs,S_part[1]),
                                                "songLen":data.songLen(songs,S_part[1]),
                                                "songWords":data.songWords(songs,S_part[1]),
                                                "whatAlbum":data.whatAlbum(album_songs,S_part[1]),
                                                "nameSearch":data.nameSearch(songs,S_part[1]),
                                                "wordSearch":data.wordSearch(songs,S_part[1]),
                                                "mostPopular":data.mostPopular(songs),
                                                "longest":data.longest(album_songs,songs)}
                                answer,S_part[1]=answer_option[S_part[0]]
                                if answer != False:
                                    sock.sendall(("RESPONSE#%s#%s#%s\n"%(S_part[0],S_part[1],answer)).encode())
                                    #needs to create a fanction that if he cant find the wanted album make answer a false and send a massage(probably the same massage)
                                    #like that:
                                else:
                                    sock.sendall(TYPE_EROR.encode())
                            elif F_part == "EXIT":
                                sock.sendall(EXIT_MSG.encode())
                                con = False
                            else:
                                sock.sendall(TYPE_EROR.encode())
                        #except KeyError:
                        #    sock.sendall(TYPE_EROR.encode())
                except ValueError:
                    sock.sendall(TYPE_EROR.encode())
                except ConnectionError:
                    con = False


if __name__ == "__main__":
    main()
