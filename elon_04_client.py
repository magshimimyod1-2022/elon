#PFP_client
import socket
def check_server_msg(server_msg):
    if server_msg[0]=="RESPONSE":
        output={"albumList":"pink floyd album: %s"%server_msg[3],
                "albumSongs":"the dark side songs: %s"%server_msg[3],
                "songLen":"the song %s length is %s minutes"%(server_msg[2],server_msg[3]),
                "songWords":"the words to the song %s:\n%s"%(server_msg[2],server_msg[3]),
                "whatAlbum":"the album '%s' have '%s' in him"%(server_msg[3],server_msg[2]),
                "nameSearch":"the album '%s' have '%s' in their name"%(server_msg[3],server_msg[2]),
                "wordSearch":"the songs '%s' have '%s' in their lyrics"%(server_msg[3],server_msg[2])}
        print(output[server_msg[1]])
    elif server_msg[0]=="ERROR":
        print(server_msg[1])
    elif server_msg[0]=="EXIT":
        print(server_msg[1])
    else:
        print(server_msg[1])
def correct_comands(client_msg):
    if client_msg!="exit()":
        str="GET#"
        client_msg=str+client_msg
    elif client_msg=="exit()":
        client_msg="EXIT#exit()"
    return client_msg.encode()


def main():
    SERVER_IP="127.0.0.1"
    SERVER_PORT=4242
    try:
        with socket.socket() as sock:
            server_adress=(SERVER_IP,SERVER_PORT)
            sock.connect(server_adress)
            server_msg=sock.recv(1024)
            print(server_msg.decode())
            client_msg=input()
            sock.sendall(client_msg.encode())
            server_msg=sock.recv(1024)
            server_msg=server_msg.decode()

            while server_msg[:4]!="menu":
                if (client_msg[:4]=="EXIT"):
                    print(server_msg[5:])
                    return #so the program stop
                else:
                    print(server_msg)
                    client_msg=input()
                    sock.sendall(client_msg.encode())
                    server_msg=sock.recv(1024)
                    server_msg=server_msg.decode()
            else:
                print(server_msg)
            client_msg=input()
            client_msg=correct_comands(client_msg)
            sock.sendall(client_msg)
            server_msg=sock.recv(1024)
            server_msg=server_msg.decode()
            server_msg=server_msg.split("#")
            check_server_msg(server_msg)
            while server_msg[0]!="EXIT":
                    server_msg=sock.recv(1024)
                    print(server_msg.decode())
                    client_msg=input()
                    client_msg=correct_comands(client_msg)
                    sock.sendall(client_msg)
                    server_msg=sock.recv(1024)
                    server_msg=server_msg.decode()
                    server_msg=server_msg.split("#")
                    check_server_msg(server_msg)
    except ConnectionRefusedError:
            print("the server is closed:( better luck next time")
        #can happened in any time sense the connection:)
    except ConnectionResetError:
            print("the server is closed:( better luck next time")






if __name__=="__main__":
    main()
