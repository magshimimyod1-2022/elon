# PFP_client
import socket


def check_server_msg(server_msg):
    """
    check server massage and print massage accordingly
    :param server_msg: the massage from the server
    :return: none
    """
    if server_msg[0] == "RESPONSE":
        if not "," in server_msg[3]:
            server_msg[3] = '"' + server_msg[3] + '"'
        output = {"albumList": "pink floyd album: %s" % server_msg[3],
                  "albumSongs": "the %s songs: %s" % (server_msg[2], server_msg[3]),
                  "songLen": "the song %s length is %s minutes" % (server_msg[2], server_msg[3]),
                  "songWords": "the words to the song %s:\n%s" % (server_msg[2], server_msg[3]),
                  "whatAlbum": "the album %s have '%s' in him" % (server_msg[3], server_msg[2]),
                  "nameSearch": "the album %s have '%s' in their name" % (server_msg[3], server_msg[2]),
                  "wordSearch": "the songs %s have '%s' in their lyrics" % (server_msg[3], server_msg[2]),
                  "mostPopular": "the most used words are\n%s" % server_msg[3],
                  "longest": "the albums sorted by length:\n%s" % server_msg[3]}
        print(output[server_msg[1]])
    elif server_msg[0] == "ERROR":
        print(server_msg[1])
    elif server_msg[0] == "EXIT":
        print(server_msg[1])
    else:
        print(server_msg[1])


def correct_comands(client_msg):
    """
    adds the right prefix so the server won't be mad
    :param client_msg: the massage with the no prefix
    :return: the massage with the prefix and in bytes
    """
    infor=""
    if client_msg!=1 and client_msg!=8 and client_msg!=9 and client_msg!=10:
        infor=input("i need you to bee more specific...\n")
    commads={1:"albumList()",
                2:"albumSongs(%s)"%infor,
                3:"songLen(%s)"%infor,
                4:"songWords(%s)"%infor,
                5:"whatAlbum(%s)"%infor,
                6:"nameSearch(%s)"%infor,
                7:"wordSearch(%s)"%infor,
                8:"mostPopular()",
                9:"longest()",
                10:"exit()"}
    client_msg=commads[client_msg]
    if client_msg != "exit()":
        str = "GET#"
        client_msg = str + client_msg
    elif client_msg == "exit()":
        client_msg = "EXIT#exit()"
    return client_msg.encode()


def main():
    SERVER_IP = "127.0.0.1"
    SERVER_PORT = 4242
    try:
        with socket.socket() as sock:
            server_adress = (SERVER_IP, SERVER_PORT)
            sock.connect(server_adress)
            server_msg = sock.recv(1024)
            print(server_msg.decode())
            client_msg = "HELO?"
            sock.sendall(client_msg.encode())
            server_msg = sock.recv(1024)
            server_msg = server_msg.decode()

            while server_msg[:4] != "menu":
                if (client_msg[:4] == "EXIT"):
                    print(server_msg[5:])
                    return  # so the program stop
                else:
                    print(server_msg)
                    client_msg = "HELO?"
                    sock.sendall(client_msg.encode())
                    server_msg = sock.recv(1024)
                    server_msg = server_msg.decode()
            else:
                print(server_msg)
            valid_input=False
            while not valid_input:
                try:
                    client_msg = int(input("what do you want? "))
                    if 11>client_msg>0:
                        valid_input=True
                    else:
                        print("you need to choose 1-10 numbers")
                except:
                    print("you need to print numbers!!(1-10)")
            client_msg = correct_comands(client_msg)
            sock.sendall(client_msg)
            server_msg = sock.recv(1024)
            server_msg = server_msg.decode()
            server_msg = server_msg.split("#")
            check_server_msg(server_msg)
            while server_msg[0] != "EXIT":
                server_msg = sock.recv(1024)
                print(server_msg.decode())
                valid_input=False
                while not valid_input:
                    try:
                        client_msg = int(input("what do you want? "))
                        if 11>client_msg>0:
                            valid_input=True
                        else:
                            print("you need to choose 1-10 numbers")
                    except:
                        print("you need to print numbers!!(1-10)")
                client_msg = correct_comands(client_msg)
                sock.sendall(client_msg)
                server_msg = sock.recv(1024)
                server_msg = server_msg.decode()
                server_msg = server_msg.split("#")
                check_server_msg(server_msg)
    except ConnectionRefusedError:
        print("the server is closed:( better luck next time")
    # can happened in any time sense the connection:)
    except ConnectionResetError:
        print("the server is closed:( better luck next time")


if __name__ == "__main__":
    main()
