# data
def albumList(album_name):
    """
    print an str of pink fluid Album
    :param album_name: a list of the albums
    :return: none-to what is supposed to put in the ()
    answer-the str
    """
    answer = ",".join(album_name)
    infor = "none"
    return answer, infor

def albumSongs(album_songs, albumName):
    """
    return the songs of an album
    :param album_songs: dictinary of album:song,song
    :param albumName: the name of the album
    :return: answer:the songs,albumName:the album
    """
    try:
        answer = ",".join(album_songs[albumName])
    except KeyError:
        answer = False
    return answer, albumName


def songLen(songs, songName):
    """
    return the length of a choosen song
    :param songs: a dictinary with al of thair song lookin like this song:(len,lyrics)
    :param songName: the song you want to get his length
    :return: answer:the song len,songName: the name of the song
    """
    try:
        answer = songs[songName][0]
    except KeyError:
        answer = False
    return answer, songName


def songWords(songs, songName):
    """
    return the lyrics of a choosen song
    :param songs: a dictinary with al of thair song lookin like this song:(len,lyrics)
    :param songName: the song you want to get his lyrics
    :return: answer:the song lyrics,songName: the name of the song
    """
    try:
        answer = songs[songName][1]
    except KeyError:
        answer = False
    return answer, songName


def whatAlbum(album_songs, songName):
    """
    finds the song's album
    :param album_songs: a dictinary of album:song,song
    :param songName: the name of the song
    :return: answer: the album's name,songName:the song name
    """
    found = False
    for album in album_songs.keys():
        if songName in album_songs[album]:
            answer = album
            found = True
    if not found:
        answer = False
    return answer, songName


def nameSearch(songs, word):
    """
    return the songs that thair name has the str
    :param songs: a dictinary with al of thair song lookin like this song:(len,lyrics)
    :param word: the str you want to search
    :return: answer(as a string):the songs that have the str,word:the str
    """
    answer = []
    found = False
    word = word.lower()
    for song in songs.keys():
        song = song.lower()
        if word in song:
            answer.append(song)
            found = True
    if found:
        return ",".join(answer), word
    else:
        return False, word


def wordSearch(songs, word):
    """
    return the songs that thair lyrics has the str
    :param songs: a dictinary with al of thair song lookin like this song:(len,lyrics)
    :param word: the str you want to search
    :return: answer(as a string):the songs that have the str,word:the str
    """
    answer = []
    found = False
    word = word.lower()
    for song in songs.keys():
        lyric = songs[song][1]
        lyric = lyric.lower()
        if word in lyric:
            answer.append(song)
            found = True
    if found:
        return ",".join(answer), word
    else:
        return False, word

def get_all_words(songs):
    """
    get all the words from all thair songs
    :param songs: dictaniray of the songs and info about them
    :return: words-all the words from all the songs
            no double- all the words with no double eords
    """
    words=[]
    no_double=[]
    lyrics=""
    for data in songs:
        lyrics=lyrics+songs[data][1]
    lyrics=lyrics.lower()
    lyrics=lyrics.replace("\n"," ")
    words=lyrics.split(" ")
    no_double=list(dict.fromkeys(words))
    return words,no_double


def mostPopular(songs):
    """
    find the 50 most used words from pink floyd lyrics
    :param songs: dictaniray of the songs and info about them
    :return: str of the 50 most used words
    """
    infor = "none"
    answer=[]
    appered={}
    times=[]
    words,no_double=get_all_words(songs)
    for data in no_double:
        times_appeard=words.count(data)
        if times_appeard not in times:
            appered[times_appeard]=[data]
            times.append(times_appeard)
        else:
            appered[times_appeard].append(data)
    times.sort()
    for i in times[::-1]:
        for lyrics in appered[i]:
            answer.append(lyrics)
    return ",".join(answer[:50]),infor

def longest(album_songs,songs):
    """
    sort the album by thair length
    :param album_songs: dictunary with the album as keys and song as values
    :param songs: dictaniray of the songs and info about them
    :return: the albums sorted by their length with their length and devided by \n
    """
    albums={}
    times=[]
    answer=""
    for album in album_songs:
        albums_lenM=0
        albums_lenS=0
        add_minutes=0
        for song in album_songs[album]:
            minute,seconds=songs[song][0].split(":")
            albums_lenM+=int(minute)
            albums_lenS+=int(seconds)
        add_minutes=albums_lenS/60
        albums_lenS=albums_lenS%60
        album_len="%d:%d"%(albums_lenM+add_minutes,albums_lenS)
        if album_len not in times:
            albums[album_len]=album
            times.append(album_len)
        else:
            #so near every thing there will be a number
            albums[album_len]+=",%s"(album[album_len])
    times.sort()
    for len in times[::-1]:
        answer="%s %s-%s\n"%(answer, albums[len],len)
    return answer,"none"