import socket
import datetime
import string
def selection_sort(a):
    """
    sort the list
    credit to C
    :param a: the sort list
    :return: the sorted list
    """
    for i in range(len(a)):
        mi = i
        
        for j in range(i + 1, len(a)):
            if float(a[j][0]) < float(a[mi][0]):
                mi = j
        
        if i != mi:
            a[i], a[mi] = a[mi], a[i]
    return a

def get_response(SERVER_IP,SERVER_PORT,sock,s_request):
    """
    get response from the server
    :param SERVER_IP: the adres of the server
    :param SERVER_PORT: the port of the server
    :param sock: supposed to represent the socket created
    :param s_request: the requst from the website
    :return: the response of the server
    """
    server_addres = (SERVER_IP, SERVER_PORT)
    sock.connect(server_addres)
    server_response = sock.recv(1024)  # so it will get the Welcom msg
    sock.sendall(s_request.encode())
    server_response = sock.recv(1024)
    server_response = server_response.decode()
    return server_response

def caculate_checksum(date, city):
    """
    caculate the checksum
    :param date: the date that was typed
    :param city: the choosen city
    :return: the checksum
    """
    day = date[:2]
    city1 = city.lower()
    alphabet = string.ascii_lowercase
    # makes an str of all the lower latters
    city_checksum = 0
    for i in city1:
        city_checksum += (alphabet.find(i)) + 1  # start with 0
    day_checksum = int(day)
    while day_checksum > 9:
        day_checksum -= 9
    day_checksum = (day_checksum + 9) * 0.01
    checksum = city_checksum + day_checksum
    return checksum


def top_10():
    SERVER_IP = "34.218.16.79"
    SERVER_PORT = 77
    file_path=r"C:\Users\test0\OneDrive\שולחן העבודה\magshimim\reshatot\תרגיל בית\capitals20.txt"
    with open(file_path, 'r') as file:
        file_str=file.read()
        file_list=file_str.split("\n")
        file_lists=[]
        city_list=[]
        for i in file_list:
            file_lists.append(i.split(","))
            city_list.append(file_lists[-1][2])
        city_tamp=[]
        for city in city_list:
            with socket.socket() as sock:
                american_date = datetime.datetime.now()
                date = "%s/%s/%s" % (american_date.strftime("%d"), american_date.strftime("%m"), american_date.strftime("%Y"))
                checksum=caculate_checksum(date,city)
                s_request = "100:REQUEST:city=%s&date=%s&checksum=%s" % (city, date, checksum)
                s_res=get_response(SERVER_IP,SERVER_PORT,sock,s_request)
                tamp_place=s_res.find("temp")
                a_place=s_res.find("&text")
                tamp = s_res[tamp_place + 5:a_place]
                city_tamp.append((tamp,city))
        city_tamp=selection_sort(city_tamp)
        city_tamp=city_tamp[::-1]#give from smalleat to biggest
        for i in range(10):
            city=city_tamp[i][1]
            tamp=city_tamp[i][0]
            str_i=str(i+1)
            comp=str_i+". "+city+","+tamp+" degrees"
            print(comp)


top_10()